const express = require('express')
const util = require('util')
const path = require('path');
const nodemailer = require('nodemailer')
const HandleBars = require('handlebars')
const base64Img = require('base64-img')

const Message = require('../models/message')
const BoardingPass = require('../models/boardingpass')

//const AncillaryService = require('../models/ancillaryservice')

const BoardPassBuilder = require('../utils/boardpassbuilder')
const ArrayHandler = require('../utils/arrayhandler')
const AttachmentHandler = require('../utils/attachmenthandler')
const MailHandler = require('../utils/mailhandler')
const WhatsappNotify = require('../utils/whatsappnotify')
const SMSNotify = require('../utils/smsnotify')
const WhatsAppMessageBuilder = require('../utils/whatsappmessagebuilder')
const SMSMessageBuilder = require('../utils/smsmessagebuilder')
const LanguageData = require('../utils/languagedata')
const FileHandler = require('../utils/filehandler')
const fileHandler = new FileHandler()

const AttachmentTemplatesDir = path.join(__dirname, '../../templates/boardingpass/boardpassattachments')

let AttachmentsDir = path.join(__dirname, '../../attachments')
const imageDir = path.join(__dirname, '../../images')

console.log('Using ATTACHMENTS_DIR: ' + AttachmentsDir)

const router = new express.Router()
const attachmentHandler = new AttachmentHandler()
const boardPassBuilder = new BoardPassBuilder()
const arrayHandler = new ArrayHandler()
const mailHandler = new MailHandler()
const languageData = new LanguageData()
const whatsappMessageBuilder = new WhatsAppMessageBuilder()
const smsMessageBuilder = new SMSMessageBuilder()
const whatsappnotify = new WhatsappNotify()
const smsNotify = new SMSNotify()

router.post('/send-boardingpass-email-v1', async (req, res) => {

    const inputMessage = new BoardingPass({
        ...req.body
    })

    //console.log(inputMessage)

    const travelData = await boardPassBuilder.getTravelData(inputMessage)

    console.log(travelData.PAXs[0].FlightLegs[0].FlightBoardingDate)

    if(!travelData.PAXs[0].FlightLegs[0].FlightBoardingDate) {
        let failureMsg = "The request cannot be processed. Boarding information required to process and issue a boarding pass... please provide a valid request"
        console.log(failureMsg)
        res.status(400).send({status: "Failed", reason: failureMsg})
    } else {
        //console.log(travelData)
        const langCode = travelData.LanguageCode.toLowerCase()

        await boardPassBuilder.genBoardPassAttachments(langCode + '_boardingpassattachment.handlebars', travelData, AttachmentTemplatesDir, AttachmentsDir, langCode + '_emd.handlebars').then(async (attachmentList) => {

            try {
                if (travelData.Recipient) {
                    console.log('sending boarding pass email to: ' + travelData.Recipient)

                    //read attachments and send the email......
                    var attachments = []
                    await arrayHandler.asyncForEach(attachmentList, async (attachment) => {
                        attachments.push(attachmentHandler.getAttachmentContent(AttachmentsDir, attachment))
                    });

                    var origin = undefined
                    travelData.PAXs.forEach(passgr => {
                        passgr.FlightLegs.forEach(leg => {
                            if (leg.FlightLegID === 1 && !origin) {
                                origin = leg.FlightOrigin
                            }
                        })
                    });

                    //get subject string from languagedata using language code
                    var emailSubject = languageData.getSubjectForLangCode(langCode)
                    emailSubject = emailSubject.replace("$$ORIGIN$$", origin)
                    emailSubject = emailSubject.replace("$$PNR$$", travelData.PNR)


                    console.log('Subject: ' + emailSubject)

                    await mailHandler.sendEmail(travelData, attachments, langCode + '_boardpassemail.handlebars', emailSubject)
                } else {
                    console.log('Email Message not required/sent...')
                }

                //send whatsapp notification
                const boardPassMessage = await whatsappMessageBuilder.buildBoardingPassMessage(travelData)

                if (travelData.WhatsAppRecipient) {
                    console.log('sending WhatsApp notification to:' + travelData.WhatsAppRecipient)
                    await whatsappnotify.notifyOnWhatsapp(boardPassMessage, travelData.WhatsAppRecipient)
                } else {
                    console.log('Whatsapp Message not required/sent...')
                }

                //TODO: build the barcoded image for whatsapp
                // const boardPassMessage = await whatsappMessageBuilder.buildBPWhatsAppMessage(travelData, AttachmentsDir)
                // console.log(boardPassMessage)
                // await whatsappnotify.notifyOnWhatsapp(boardPassMessage, travelData.WhatsAppRecipient)

                //send SMS notification
                const smsBoardPassMessage = await smsMessageBuilder.buildBoardingPassMessage(travelData)

                if (travelData.smsRecipient) {
                    console.log('sending SMS notification to:' + travelData.smsRecipient)
                    await smsNotify.notifyOnSMS(smsBoardPassMessage, travelData.smsRecipient)
                } else {
                    console.log('SMS Message not required/sent...')
                }


                res.status(201).send({ status: 'success' })
            } catch (e) {
                console.log("Error...." + e)
                res.status(400).send(e)
            }

        }).catch((err) => {
            console.log(err)
        })
    }
})

router.post('/send-ancillary-email-v1', async (req, res) => {
    const inputMessage = new BoardingPass({
        ...req.body
    })

    const travelData = await boardPassBuilder.getTravelData(inputMessage)

    const langCode = travelData.LanguageCode.toLowerCase()

    await boardPassBuilder.genEMDMessage(travelData, AttachmentTemplatesDir, AttachmentsDir, langCode + '_emd.handlebars').then(async (ancillarySet) => {
        try {
            if (travelData.Recipient) {
                var emailSubject = languageData.getAncillarySubjectForLangCode(langCode)
                var paxName = ''
                travelData.PAXs.forEach(pass => {
                    if (pass.PAXIsLead) {
                        paxName = pass.PAXTitle + ' ' + pass.PAXFirstName + ' ' + pass.PAXLastName
                    }
                })
                emailSubject = emailSubject.replace("$$PAXNAME$$", paxName)

                console.log(emailSubject)

                //console.log(ancillarySet)
                var airplaneLogo = path.join(imageDir, 'JRNY_Fare choices.png')
                if(langCode === 'ar') {
                    airplaneLogo = path.join(imageDir, 'AR_JRNY_Fare choices.png')
                }
                
                ancillarySet.aircraft = base64Img.base64Sync(airplaneLogo)

                var attachments = []
                await arrayHandler.asyncForEach(ancillarySet.emdAttachments, async (attachment) => {
                    attachments.push(attachmentHandler.getAttachmentContent(AttachmentsDir, attachment))
                });
                
                console.log('Sending ancillary email to: ' + ancillarySet.Recipient)
                mailHandler.sendEmail(ancillarySet, attachments, '/ancillary/' + langCode + '_ancillary.handlebars', emailSubject)
            } else {
                console.log('Email Message not required/sent...')
            }

            if (ancillarySet.WhatsAppRecipient) {
                const ancillaryMessage = await whatsappMessageBuilder.buildAncillaryMessage(ancillarySet)

                console.log('sending WhatsApp notification to:' + ancillarySet.WhatsAppRecipient)
                await whatsappnotify.notifyOnWhatsapp(ancillaryMessage, ancillarySet.WhatsAppRecipient)
            } else {
                console.log('Whatsapp Message not required/sent...')
            }

            //send SMS notification
            const smsBoardPassMessage = await smsMessageBuilder.buildAncillaryMessage(ancillarySet)

            if (travelData.smsRecipient) {
                console.log('sending SMS notification to:' + travelData.smsRecipient)
                await smsNotify.notifyOnSMS(smsBoardPassMessage, travelData.smsRecipient)
            } else {
                console.log('SMS Message not required/sent...')
            }

            res.status(201).send({ status: 'success' })
        } catch (e) {
            console.log("Error...." + e)
            res.status(400).send(e)
        }
    }).catch((err) => {
        console.log(err)
    })

})


router.post('/debug-file-save-v1', async (req, res) => {
    let output = '<html><head></head><body><h1>Hello... this is a test process to check file save...Take 3</h1></body></html>'

    console.log('Contents being written to the file: ' + output)

    try {
        await fileHandler.writeFileAsPDF(AttachmentsDir + '/debug-file-save.pdf', output)

        console.log('Success on creating the file...')

        res.status(201).send({ status: 'success' })
    } catch (e) {
        console.log('Error: ' + e)
        res.status(400).send(e)
    }
})


module.exports = router