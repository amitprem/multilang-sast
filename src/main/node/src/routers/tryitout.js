const express = require('express')
const TryMeSchema = require('../models/trymeschema')
const nodemailer = require('nodemailer')
const HandleBars = require('handlebars')
const WhatsappNotify = require('../utils/whatsappnotify')
const SMSNotify = require('../utils/smsnotify')

const router = new express.Router()
const whatsappnotify = new WhatsappNotify()
const smsNotify = new SMSNotify()

router.post('/tryme/email', async (req, res) => {

    const inputMessage = new TryMeSchema({
        ...req.body
    })

    console.log(inputMessage)

    try {
        const emailSubject = 'Etihad Notification Engine - TryMe Email Message'
        const htmlOutput = '<html><head></head><body><h1>Hello... Test Email triggered by the TryMeOut API</h1><br><br>'
            + 'Message : {{EmailMessage}}'
            + '</body></html>'

        const template = await HandleBars.compile(htmlOutput);

        const output = await template({
            "EmailMessage": inputMessage.MessageData.Message
        })
        
        const transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true, // use SSL
            auth: {
                user: 'agrahara.subbu@zohomail.com',
                pass: 'Logmeinn@13'
            }
        })

        let mailOptions = {
            from: 'agrahara.subbu@zohomail.com',
            to: inputMessage.Recipients.EmailRecipient,
            subject: emailSubject,
            html: output
        }

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log('Error Occured' + error);
                throw new Error(error);
            } else {
                console.log('Email message Sent: ' + info.response);
                return info
            }
        });

        res.status(201).send({ status: 'success' })
    } catch (e) {
        console.log("Error...." + e)
        res.status(400).send(e)
    }

})


router.post('/tryme/whatsapp', async (req, res) => {

    const inputMessage = new TryMeSchema({
        ...req.body
    })

    console.log(inputMessage)

    try {
        await whatsappnotify.notifyOnWhatsapp(inputMessage.MessageData.Message, inputMessage.Recipients.WhatsAppRecipient)

        res.status(201).send({ status: 'success' })
    } catch (e) {
        console.log("Error...." + e)
        res.status(400).send(e)
    }

})

router.post('/tryme/sms', async (req, res) => {

    const inputMessage = new TryMeSchema({
        ...req.body
    })

    console.log(inputMessage)

    try {
        await smsNotify.notifyOnSMS(inputMessage.MessageData.Message, inputMessage.Recipients.SMSRecipient)

        res.status(201).send({ status: 'success' })
    } catch (e) {
        console.log("Error...." + e)
        res.status(400).send(e)
    }

})

module.exports = router