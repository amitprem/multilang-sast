const express = require('express')
const path = require('path');
//require("./db/mongoose.js")

//const User = require('./models/user')

//const userRouter = require('./routers/user')
const mailRouter = require('./routers/mail')
const tryMeRouter = require('./routers/tryitout')

const PublicDir = path.join(__dirname, '../public')

const app = express()
const port = process.env.PORT || 80

app.use(express.static(PublicDir))

app.use(express.json())
//app.use(userRouter)
app.use(mailRouter)
app.use(tryMeRouter)

console.log('Using PORT: ' + port)


app.listen(port, () => {
    console.log('Meghadoot v1 Started... listening on port ' + port)
})
