const mongoose = require('mongoose')

const trymeSchema = new mongoose.Schema({
    MessageData: {
        Message: { type: String, required: true, trim: true },
        Language: { type: String, required: true, trim: true }
    },
    Recipients: {
        EmailRecipient: { type: String, required: false },
        WhatsAppRecipient: { type: String, required: false },
        SMSRecipient: { type: String, required: false}
    }
})


const TryMeSchema = mongoose.model('TryMeSchema', trymeSchema)

module.exports = TryMeSchema