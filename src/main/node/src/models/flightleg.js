const dateFormat = require('dateformat');
const acTypes = require('../utils/aircrafttypes')

function FlightLeg(flightLegID) {
    this.FlightLegID = parseInt(flightLegID)
}

FlightLeg.prototype.setFlightNum = function( flightCode, flightNumber ) {
    this.FlightNum = flightCode + flightNumber
}

FlightLeg.prototype.setOrigin = function( departureCity ) {
    this.FlightOrigin = departureCity
}

FlightLeg.prototype.setDestination = function( arrivalCity ) {
    this.FlightDestination = arrivalCity
}

FlightLeg.prototype.setOriginCityName = function( departureCityName ) {
    this.FlightOriginCityName = departureCityName
}

FlightLeg.prototype.setDestinationCityName = function( arrivalCityName ) {
    this.FlightDestinationCityName = arrivalCityName
}

FlightLeg.prototype.setDepartureGate = function( deptGate ) {
    this.FlightDepGate = deptGate
}
    
FlightLeg.prototype.setBoardingZone = function( boardingZone ) {
    this.FlightBoardingZone = boardingZone
}

FlightLeg.prototype.setTicketNumber = function( ticketNumber ) {
    this.TicketNumber = ticketNumber
}

FlightLeg.prototype.setSequenceNumber = function( sequenceNumber ) {
    this.SequenceNumber = sequenceNumber
}

FlightLeg.prototype.setSeatNumber = function( seatNum ) {
    this.PAXSeatNum = seatNum
}

FlightLeg.prototype.setEMDs = function( emds ) {
    this.EMDs = emds
}

FlightLeg.prototype.setAircraftType = function( aircraftType ) {
    if(aircraftType) {
        this.AircraftType = aircraftType
        acTypeInfo = (new acTypes()).getAircraftTypeInfo(aircraftType)
        if(acTypeInfo) {
            this.AircraftICAOCode = acTypeInfo.ICAOCode
            this.AircraftTypeDescription = acTypeInfo.Description
        }
    }
}

FlightLeg.prototype.setDepartureDate = function( departureDate ) {
    formattedDate = formatDate(departureDate)
    this.FlightDepartureDate = formattedDate[0]
    this.FlightDepartureTime = formattedDate[1]
}
 
FlightLeg.prototype.setArrivalDate = function( arrivalDate ) {
    formattedDate = formatDate(arrivalDate)
    this.FlightArrivalDate = formattedDate[0]
    this.FlightArrivalTime = formattedDate[1]
}
 
FlightLeg.prototype.setBoardingTime = function( boardingTime ) {
    formattedDate = formatDate(boardingTime)    
    this.FlightBoardingDate = formattedDate[0]
    this.FlightBoardingTime = formattedDate[1]
}

FlightLeg.prototype.setTravelClass = function( travelClass ) {
    this.PAXTravelClass = travelClass
}

FlightLeg.prototype.setBarcode1 = function( barcode ) {
    this.LegBarcode1 = barcode
}

FlightLeg.prototype.setBarcode2 = function( barcode ) {
    this.LegBarcode2 = barcode
}

function formatDate(inputDateString) {
    if(inputDateString) {
        let inputDate = dateFormat(inputDateString.split("T")[0], "fullDate")
        let inputTime = inputDateString.split("T")[1]
        return [inputDate, inputTime]
    } else {
        return ["", ""]
    }
}

module.exports = FlightLeg