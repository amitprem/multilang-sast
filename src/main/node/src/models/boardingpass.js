const mongoose = require('mongoose')

const boardpassSchema = new mongoose.Schema({
    Reservation: {
        LanguageCode: { type: String, required: true, trim: true },
        PNR: { type: String, required: true, trim: true },
        ReservationAgent: { type: String, trim: true },
        PAXs: {
            PAX: [{
                FirstName: { type: String, required: true },
                LastName: { type: String, required: true },
                Title: { type: String, required: true },
                Infant: { type: Boolean, default: false },
                LeadPax: { type: Boolean, required: true },
                attributes: {
                    Id: { type: String, required: true }
                }
            }]
        },
        Flights: {
            Flight: [{
                FlightCode: {
                    FlightNumber: { type: String, required: true },
                    Code: { type: String, required: true }
                },
                OperatorFlightCode: {
                    FlightNumber: { type: String },
                    Code: { type: String }
                },
                DepartureDate: { type: String },
                DepartureCity: { type: String },
                ArrivalDate: { type: String },
                ArrivalCity: { type: String },
                DepartureTerminal: { type: String },
                AircraftType: { type: String },
                FlightUpdate: {
                    DeptGate: { type: String },
                    BoardingTime: { type: String },
                    BoardingZone: { type: String }
                },
                attributes: {
                    FlightLeg: { type: String, required: true }
                }
            }]
        },
        PAXFlights: {
            PAXFlight: [{
                SeatNumber: { type: String },
                ClassOfService: { type: String },
                Ticket: {
                    TicketNumber: { type: String },
                    IssueDate: { type: String },
                    IssueAgency: { type: String },
                    PlatingCarrier: { type: String }
                },
                Remarks: {
                    Remark: [{
                        Type: { type: String },
                        Remark: { type: String }
                    }]
                },
                EMDs: {
                    EMD: [{
                        EmdNumber: { type: String },
                        EmdType: { type: String },
                        BasePrice: { type: String },
                        TaxAmount: { type: String },
                        BaseCurrency: { type: String },
                        DateOfIssuance: { type: String },
                        IssueAgency: { type: String },
                        Description: { type: String },
                        Company: { type: String },
                        FormOfPayment: { type: String },
                        TotalPrice: { type: String },
                        TotalCurrency: { type: String }
                    }]
                },
                SequenceNo: { type: String },
                TsaPreCheck: { type: String },
                Barcode1: { type: String },
                Barcode2: { type: String },
                attributes: {
                    PAXId: { type: String },
                    FlightLeg: { type: String }
                }
            }]
        }
    },
    Recipients: {
        Name: { type: String, required: true },
        EmailRecipient: { type: String, required: true },
        WhatsAppRecipient: { type: String, required: false },
        SMSRecipient: { type: String, required: false}
    },
    ownedBy: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }
}, {
    timestamps: true
})

const BoardingPass = mongoose.model('BoardingPass', boardpassSchema)

module.exports = BoardingPass