const dateFormat = require('dateformat');
var fs = require('fs');
const path = require('path')
const base64Img = require('base64-img')

const configDir = path.join(__dirname, '../../config')
const imageDir = path.join(__dirname, '../../images')

const ancillaryRefsJSONFile = path.join(configDir, 'ancillaryrefs.json')

//EMD related fields start

function EMD(emdNumber) {
    this.EmdNumber = emdNumber
}

EMD.prototype.setPAXName = function( paxName ) {
    this.EmdPAXName = paxName
}

EMD.prototype.setEmdType = function( emdType, langCode ) {
    this.EmdType = emdType

    ancRefData = getEMDTypeData(emdType, langCode)

    const emdTypeImage = path.join(imageDir, ancRefData.ANCImage)
    this.EmdImage = base64Img.base64Sync(emdTypeImage)

    this.EmdDetailsMessage = ancRefData.ANCDetailsMessage
}

EMD.prototype.setEmdBasePrice = function( emdBasePrice ) {
    this.EmdBasePrice = emdBasePrice
}

EMD.prototype.setEmdTaxAmount = function( emdTaxAmount ) {
    this.EmdTaxAmount = emdTaxAmount
}

EMD.prototype.setEmdBaseCurrency = function( emdBaseCurrency ) {
    this.EmdBaseCurrency = emdBaseCurrency
}

EMD.prototype.setEmdDateOfIssuance = function( emdIssueDate ) {
    formattedDate = formatDate(emdIssueDate)    
    this.EmdDateOfIssuance = formattedDate[0]
}

EMD.prototype.setEmdIssueAgency = function( emdIssueAgency ) {
    this.EmdIssueAgency = emdIssueAgency
}

EMD.prototype.setEmdDescription = function( emdDescription ) {
    this.EmdDescription = emdDescription
}

EMD.prototype.setEmdCompany = function( emdCompany ) {
    this.EmdCompany = emdCompany
}

EMD.prototype.setEmdFormOfPayment = function( emdFormOfPayment ) {
    this.EmdFormOfPayment = emdFormOfPayment
}

EMD.prototype.setEmdTotalPrice = function( emdTotalPrice ) {
    this.EmdTotalPrice = emdTotalPrice
}

EMD.prototype.setEmdTotalCurrency = function( emdTotalCurrency ) {
    this.EmdTotalCurrency = emdTotalCurrency
}


//EMD related fields end

function formatDate(inputDateString) {
    if(inputDateString) {
        let inputDate = dateFormat(inputDateString.split("T")[0], "fullDate")
        let inputTime = inputDateString.split("T")[1]
        return [inputDate, inputTime]
    } else {
        return ["", ""]
    }
}

function getEMDTypeData(emdType, langCode) {
    var contents = fs.readFileSync(ancillaryRefsJSONFile);
    //console.log(contents);

    var ANCILLARY_REFS = JSON.parse(contents)
    //console.log('Finding subject for: ' + languageCode)
    let ancillaryRefData = ANCILLARY_REFS.Data.filter(x => x.lang === langCode)

    //console.log(ancillaryRefData[0])

    let ancillaryTypeData = ancillaryRefData[0].ancData.filter(a => a.ANCType === emdType)
    
    return ancillaryTypeData[0]
}

module.exports = EMD