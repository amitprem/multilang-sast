function Passenger() {
    this.createFlightLegs()
}

Passenger.prototype.createFlightLegs = function() {
    this.FlightLegs = []
}

Passenger.prototype.addFlightLeg = function( newFlightLeg ) {
    this.FlightLegs.push( newFlightLeg )
}

module.exports = Passenger