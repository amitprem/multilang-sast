const mongoose = require('mongoose')

const messageSchema = new mongoose.Schema({
    Reservation: {
        Recipient: { type: String, required: true },
        PNR: { type: String, required: true },
        LanguageCode: { type: String },
        ReservationAgent: { type: String },
        PAXs: [{
            PAXID: { type: String, required: true },
            PAXFirstName: { type: String, required: true },
            PAXLastName: { type: String, required: true },
            PAXTitle: { type: String, required: true },
            PAXInfant: { type: Boolean, default: false },
            PAXIsLead: { type: Boolean, required: true },
            FlightLegs: [{
                FlightLegID: { type: String, required: true },
                FlightNum: { type: String, required: true },
                FlightOrigin: { type: String, required: true },
                FlightDestination: { type: String, required: true },
                FlightDepartureDate: { type: String, required: true },
                FlightDepartureTime: { type: String, required: true },
                FlightArrivalDate: { type: String, required: true },
                FlightArrivalTime: { type: String, required: true },
                FlightDepGate: { type: String, required: true },
                FlightBoardingDate: { type: String, required: true },
                FlightBoardingTime: { type: String, required: true },
                FlightBoardingZone: { type: String, required: true },
                PAXSeatNum: { type: String, required: true },
                PAXTravelClass: { type: String, required: true }
            }]
        }]
    },
    ownedBy: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }
}, {
    timestamps: true
})

const Message = mongoose.model('Message', messageSchema)

module.exports = Message