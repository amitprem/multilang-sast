

function AncillaryService () {

}

AncillaryService.prototype.setEmailRecipient = function(recipient) {
    this.Recipient = recipient
}

AncillaryService.prototype.setWhatsAppRecipient = function(recipient) {
    this.WhatsAppRecipient = recipient
}

AncillaryService.prototype.setPNR = function(pnr) {
    this.PNR = pnr
}

AncillaryService.prototype.setPAXName = function(paxName) {
    this.PAXName = paxName
}

AncillaryService.prototype.setFlightLegs = function(flightLegs) {
    this.flightLegs = flightLegs
}

AncillaryService.prototype.setEMDs = function(emds) {
    this.EMDs = emds
}

AncillaryService.prototype.setEMDAttachments = function(emdAttachs) {
    this.emdAttachments = emdAttachs
}

module.exports = AncillaryService