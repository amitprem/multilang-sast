
function TravelData(emailRecipient, PNR, languageCode, reservationAgent, whatsappRecipient, smsRecipient) {
    this.Recipient = emailRecipient
    this.PNR = PNR
    this.LanguageCode = languageCode
    this.ReservationAgent = reservationAgent
    this.WhatsAppRecipient = whatsappRecipient
    this.smsRecipient = smsRecipient

    this.createPAXs()
}

TravelData.prototype.createPAXs = function() {
    this.PAXs = []
}

TravelData.prototype.addPAX = function( passenger ) {
    this.PAXs.push( passenger )
}

module.exports = TravelData;