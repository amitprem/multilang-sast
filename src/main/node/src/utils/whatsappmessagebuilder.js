const HandleBars = require('handlebars')
const FileHandler = require('../utils/filehandler')
const path = require('path');
//var webshot = require('webshot');
//const base64Img = require('base64-img')

const fileHandler = new FileHandler()

const TemplatesDir = path.join(__dirname, '../../templates')

function WhatsAppMessageBuilder() {

}


WhatsAppMessageBuilder.prototype.buildBoardingPassMessage = async (inputMessage) => {
    //console.log(inputMessage)
    let whatsappMessage = 'Hello... Please find your travel details below: \nPNR:' + inputMessage.PNR
    inputMessage.PAXs.forEach(passenger => {
        whatsappMessage = whatsappMessage + '\n---------------------------\n' + passenger.PAXTitle + '. ' + passenger.PAXFirstName + ' ' + passenger.PAXLastName
        passenger.FlightLegs.forEach(flightleg => {
            //console.log(flightleg)
            whatsappMessage = whatsappMessage + '\n---------------------------'
                + '\nTravel Date: ' + flightleg.FlightDepartureDate
                + '\nLeg: #' + flightleg.FlightLegID
                + '\nTravelling From: ' + flightleg.FlightOrigin + '; and Travelling To: ' + flightleg.FlightDestination
                + '\nBoarding at Origin: ' + flightleg.FlightBoardingDate + ' at ' + flightleg.FlightBoardingTime
                + '\nDeparture at Origin: ' + flightleg.FlightDepartureDate + ' at ' + flightleg.FlightDepartureTime
                + '\nArrival at Destination: ' + flightleg.FlightArrivalDate + ' at ' + flightleg.FlightArrivalTime
        })
    });

    return whatsappMessage
}

WhatsAppMessageBuilder.prototype.buildAncillaryMessage = async (ancillaryData) => {
    let whatsappMessage = 'Hello... Your ancillary purchase for PNR:' + ancillaryData.PNR
    ancillaryData.EMDs.forEach(emd => {
        whatsappMessage = whatsappMessage + '\n---------------------------'
            + '\nDocument number: ' + emd.EmdNumber
            + '\nGuest name: ' + emd.EmdPAXName
            + '\nProduct purchased: ' + emd.EmdDescription
            + '\nPaid through: ' + emd.EmdFormOfPayment
            + '\nTotal price: ' + emd.EmdTotalCurrency + ' ' + emd.EmdTotalPrice
            + '\n---------------------------'
    })

    return whatsappMessage
}



WhatsAppMessageBuilder.prototype.buildBPWhatsAppMessage = async (inputMessage, attachmentsDir) => {

    //const hbsBuffer = await fileHandler.readFile(path.join(TemplatesDir, handlebarfile))
    const hbsBuffer = await fileHandler.readFile(path.join(TemplatesDir, 'whatsappmessage.handlebars'))

    const template = await HandleBars.compile(hbsBuffer);

    const output = await template({
        "Reservation": inputMessage,
        "allowProtoMethodsByDefault": true
    })

    const imageURL = await prepareWhatsAppImage(output, attachmentsDir)


    return imageURL
}

async function prepareWhatsAppImage(htmlOutput, attachmentsDir) {
    var webshotOptions = {
        siteType: 'html',
        shotSize: {
            width: 165,
            height: 246
        },
        shotOffset: {
            bottom: 0
        },
        defaultWhiteBackground: true,
        streamType: 'jpg',
        customCSS: ""
    };

    var filename = path.join(attachmentsDir, guid() + '.jpg')
    console.log(filename)
    var imageBuffer = ''
    // await webshot(htmlOutput, filename, webshotOptions, function (error) {
    //     if (error) {
    //         console.log(error)
    //     } else {
    //         console.log('Reading file: ' + filename)
    //         imageBuffer = fileHandler.readFileForAttachment(filename)
    //     }
    // })

    console.log('ImageBuffer: ' + imageBuffer)

    return imageBuffer
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

module.exports = WhatsAppMessageBuilder;