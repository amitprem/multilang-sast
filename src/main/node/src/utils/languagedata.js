var fs = require('fs');
const path = require('path')

const configDir = path.join(__dirname, '../../config')
const bpsubjectsJSONFile = path.join(configDir, 'languagestrings.json')

function LanguageData() {

}

LanguageData.prototype.getSubjectForLangCode = function (languageCode) {
    var languageData = loadLanguageData(languageCode)
    let subjectString = ''
    if (languageData) {
        //console.log('Found subject: ' + languageData)
        subjectString = languageData.Subject;
    }

    return subjectString
}

LanguageData.prototype.getAncillarySubjectForLangCode = function (languageCode) {
    var languageData = loadLanguageData(languageCode)
    let subjectString = ''
    if (languageData) {
        //console.log('Found subject: ' + languageData)
        subjectString = languageData.AncillarySubject;
    }

    return subjectString
}

function loadLanguageData(languageCode) {

    var contents = fs.readFileSync(bpsubjectsJSONFile);
    //console.log(contents);

    var LANGDATA_CODE_MAP = JSON.parse(contents)
    //console.log('Finding subject for: ' + languageCode)
    let languageData = LANGDATA_CODE_MAP.Data.filter(x => x.Code === languageCode)
    
    return languageData[0]
}

module.exports = LanguageData