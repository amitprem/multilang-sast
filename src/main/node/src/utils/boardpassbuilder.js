const HandleBars = require('handlebars')
const pdf = require("html-pdf")
const base64Img = require('base64-img')
const path = require('path')

const ArrayHandler = require('./arrayhandler')
const FileHandler = require('./filehandler')
const BoardPass = require('../models/boardpass')

const TravelData = require('../models/traveldata')
const Traveller = require('../models/passenger')
const FlightLeg = require('../models/flightleg')
const EMD = require('../models/emd')
const PAXClassCode = require('./classcodes')
const AirportCityCode = require('./airportcitycodes')
const AncillaryService = require('../models/ancillaryservice')

const JsBarcode = require('jsbarcode');
const { createCanvas, createImageData } = require('canvas')

const createPDF417 = require('./pdf417-min')

const fileHandler = new FileHandler()
const arrayHandler = new ArrayHandler()
const airportCityCode = new AirportCityCode()

function BoardPassBuilder() {

}

BoardPassBuilder.prototype.getTravelData = async (inputMessage) => {
    let airportName = ''
    let travelData = new TravelData(inputMessage.Recipients.EmailRecipient,
        inputMessage.Reservation.PNR,
        inputMessage.Reservation.LanguageCode,
        null,
        inputMessage.Recipients.WhatsAppRecipient,
        inputMessage.Recipients.SMSRecipient)

    inputMessage.Reservation.PAXs.PAX.forEach(inputPAX => {
        let traveller = new Traveller()
        paxID = parseInt(inputPAX.attributes.Id)
        traveller.PAXID = paxID
        traveller.PAXFirstName = inputPAX.FirstName
        traveller.PAXLastName = inputPAX.LastName
        traveller.PAXTitle = inputPAX.Title
        traveller.PAXInfant = inputPAX.Infant
        traveller.PAXIsLead = inputPAX.LeadPax

        inputMessage.Reservation.Flights.Flight.forEach(flight => {

            const legID = parseInt(flight.attributes.FlightLeg)
            let seatNum = ""
            let classOfTravel = ""
            let ticketNumber = ""
            let sequenceNumber = ""
            let barcode1 = ""
            let barcode2 = ""

            var EMDsForLeg = []
            inputMessage.Reservation.PAXFlights.PAXFlight.forEach(paxFlight => {
                if (paxFlight.attributes.PAXId == paxID && paxFlight.attributes.FlightLeg == legID) {
                    seatNum = paxFlight.SeatNumber
                    classOfTravel = paxFlight.ClassOfService
                    ticketNumber = paxFlight.Ticket.TicketNumber
                    sequenceNumber = paxFlight.SequenceNo
                    barcode1 = paxFlight.Barcode1
                    barcode2 = paxFlight.Barcode2
                    //populate EMD data - start
                    if (paxFlight.EMDs) {
                        paxFlight.EMDs.EMD.forEach(doco => {
                            let emdDoc = new EMD(doco.EmdNumber)
                            emdDoc.setPAXName(inputPAX.Title + ' ' + inputPAX.FirstName + ' ' + inputPAX.LastName)
                            emdDoc.setEmdType(doco.EmdType, inputMessage.Reservation.LanguageCode.toLowerCase())
                            emdDoc.setEmdBasePrice(doco.BasePrice)
                            emdDoc.setEmdTaxAmount(doco.TaxAmount)
                            emdDoc.setEmdBaseCurrency(doco.BaseCurrency)
                            emdDoc.setEmdDateOfIssuance(doco.DateOfIssuance)
                            emdDoc.setEmdIssueAgency(doco.IssueAgency)
                            emdDoc.setEmdDescription(doco.Description)
                            emdDoc.setEmdCompany(doco.Company)
                            emdDoc.setEmdFormOfPayment(doco.FormOfPayment)
                            emdDoc.setEmdTotalPrice(doco.TotalPrice)
                            emdDoc.setEmdTotalCurrency(doco.TotalCurrency)

                            EMDsForLeg.push(emdDoc)
                        })
                    }
                    //populate EMD data - end
                }
            })

            let travelLeg = new FlightLeg(legID)
            travelLeg.setFlightNum(flight.FlightCode.Code, flight.FlightCode.FlightNumber)
            travelLeg.setOrigin(flight.DepartureCity)
            airportName = airportCityCode.getCityFromAirportCode(flight.DepartureCity)
            travelLeg.setOriginCityName(airportName.toUpperCase())
            travelLeg.setDestination(flight.ArrivalCity)
            airportName = airportCityCode.getCityFromAirportCode(flight.ArrivalCity)
            travelLeg.setDestinationCityName(airportName.toUpperCase())
            travelLeg.setDepartureDate(flight.DepartureDate)
            travelLeg.setArrivalDate(flight.ArrivalDate)
            travelLeg.setAircraftType(flight.AircraftType)
            travelLeg.setDepartureGate(flight.FlightUpdate.DeptGate)
            travelLeg.setBoardingTime(flight.FlightUpdate.BoardingTime)
            travelLeg.setBoardingZone(flight.FlightUpdate.BoardingZone)

            //go through PAXFlights and get SeatNum and TravelClass
            travelLeg.setSeatNumber(seatNum)
            classOfTravel = new PAXClassCode().getClassOfTravel(classOfTravel)
            travelLeg.setTravelClass(classOfTravel)
            travelLeg.setSequenceNumber(sequenceNumber)
            travelLeg.setTicketNumber(ticketNumber)
            travelLeg.setBarcode1(barcode1)
            travelLeg.setBarcode2(barcode2)
            travelLeg.setEMDs(EMDsForLeg)

            traveller.addFlightLeg(travelLeg)
        })

        travelData.addPAX(traveller)
    })

    return travelData
}

BoardPassBuilder.prototype.genBoardPassAttachments = async (bpAttachHandleBarFile, travelData, AttachmentTemplatesDir, AttachmentsDir, emdHandleBarFile) => {
    const imageDir = path.join(__dirname, '../../images')

    const etihadLogo = path.join(imageDir, 'etihad-choose-well.png')
    const airplaneLogo = path.join(imageDir, 'airplane.png')
    const separator = path.join(imageDir, 'cut-page.png')

    var legBoardPassContent = await getSingleLegData(travelData.PAXs, travelData.PNR)

    var attachments = []

    await arrayHandler.asyncForEach(legBoardPassContent, async (pass) => {

        var passName = 'Etihad Airways ' + pass.PNR + '_' + pass.PAXFirstName + ' ' + pass.PAXLastName + '_' + pass.FlightNum + '.pdf'
        var targetFile = AttachmentsDir + '/' + passName

        //Prepare barcode
        var barcodeURL = ''
        console.log('3: ' + pass.barcode1)
        if (pass.barcode1) {
            console.log('Found Barcode1; Generating barcode for data supplied in Barcode1')
            barcodeURL = await generateBarcode(pass.barcode1)
        }
        else {
            console.log('Barcode1 Not Found; Generating default barcode')
            //TODO: Construct the string based on the rules set for the barcode1 attribute in the input.
            barcodeURL = await generateBarcode(pass.PNR + ':' + pass.PAXFirstName + ':' + pass.PAXLastName + ':' + pass.FlightNum + ':' + pass.PAXSeatNum)
        }

        //prepare boardingpass as attachment
        const hbsBuffer = await fileHandler.readFile(path.join(AttachmentTemplatesDir, bpAttachHandleBarFile))

        const template = await HandleBars.compile(hbsBuffer);

        const output = await template({
            "PAX": pass,
            "etihadLogo": base64Img.base64Sync(etihadLogo),
            "barcode": barcodeURL,
            "plane": base64Img.base64Sync(airplaneLogo),
            "separator": base64Img.base64Sync(separator)
        })

        await fileHandler.writeFileAsPDF(targetFile, output)

        attachments.push(passName)
    })


    //Receipt preparation as attachment
    var EMDList = await getConsolidatedEMDList(travelData.PAXs)
    //console.log(EMDList)
    if(EMDList && (EMDList.length > 0)) {
        const emdBuffer = await fileHandler.readFile(path.join(AttachmentTemplatesDir, emdHandleBarFile))

        const emdTemplate = await HandleBars.compile(emdBuffer);

        const output = await emdTemplate({
            "PNR": travelData.PNR,
            "etihadLogo": base64Img.base64Sync(etihadLogo),
            "emdInfo": { "EMDs": EMDList }
        })

        var receiptName = 'Receipt details for ' + travelData.PNR + '.pdf'
        var targetFile = AttachmentsDir + '/' + receiptName
        await fileHandler.writeFileAsPDF(targetFile, output)

        attachments.push(receiptName)
    }

    return attachments
}

BoardPassBuilder.prototype.genEMDMessage = async (travelData, AttachmentTemplatesDir, AttachmentsDir, emdAttachmentHandleBarFile) => {

    const imageDir = path.join(__dirname, '../../images')

    const etihadLogo = path.join(imageDir, 'etihad-choose-well.png')
    const airplaneLogo = path.join(imageDir, 'airplane.png')
    const separator = path.join(imageDir, 'cut-page.png')

    var attachments = []

    //Receipt preparation as attachment
    var EMDList = await getConsolidatedEMDList(travelData.PAXs)
    
    if(EMDList) {
        const emdBuffer = await fileHandler.readFile(path.join(AttachmentTemplatesDir, emdAttachmentHandleBarFile))

        const emdTemplate = await HandleBars.compile(emdBuffer);

        const output = await emdTemplate({
            "PNR": travelData.PNR,
            "etihadLogo": base64Img.base64Sync(etihadLogo),
            "emdInfo": { "EMDs": EMDList }
        })

        var receiptName = 'Receipt details for ' + travelData.PNR + '.pdf'
        var targetFile = AttachmentsDir + '/' + receiptName
        await fileHandler.writeFileAsPDF(targetFile, output)

        attachments.push(receiptName)
    }

    var ancillarySet = new AncillaryService()
    ancillarySet.setPNR(travelData.PNR)
    ancillarySet.setEmailRecipient(travelData.Recipient)
    ancillarySet.setWhatsAppRecipient(travelData.WhatsAppRecipient)

    travelData.PAXs.forEach(pass => {
        if(pass.PAXIsLead)
            ancillarySet.setPAXName(pass.PAXTitle + ' ' + pass.PAXLastName)
    })

    ancillarySet.setFlightLegs(travelData.PAXs[0].FlightLegs)
    ancillarySet.setEMDs(EMDList)
    ancillarySet.setEMDAttachments(attachments)

    return ancillarySet
}


async function generateBarcode(text) {
    const blockWidth = 100
    const blockHeight = 20

    console.log('Generating Bar Code.....')

    const canvas = createCanvas(150, 50)
    const PDF417 = createPDF417();
    PDF417.init(text);
    const barcodeMatrix = PDF417.getBarcodeArray();
    canvas.width = blockWidth * barcodeMatrix.num_cols;
    canvas.height = blockHeight * barcodeMatrix.num_rows;
    await drawBarcode(canvas, barcodeMatrix, blockWidth, blockHeight);
    return canvas.toDataURL();
}

async function drawBarcode(canvas, barcodeMatrix, blockWidth, blockHeight) {
    const ctx = canvas.getContext('2d');
    let positionY = 0;
    for (let row = 0; row < barcodeMatrix.num_rows; row += 1) {
        let positionX = 0;
        for (let col = 0; col < barcodeMatrix.num_cols; col += 1) {
            if (barcodeMatrix.bcode[row][col] === '1') {
                ctx.fillStyle = '#000';
            } else {
                ctx.fillStyle = '#FFF';
            }
            ctx.fillRect(positionX, positionY, blockWidth, blockHeight);
            positionX += blockWidth;
        }
        positionY += blockHeight;
    }
}

async function getConsolidatedEMDList(allPassAndLegs) {
    var EMDs = []
    allPassAndLegs.forEach(pass => {
        pass.FlightLegs.forEach(flightLeg => {
            flightLeg.EMDs.forEach(emd => {
                EMDs.push(emd)
            })
        })
    })

    return EMDs
}

async function getSingleLegData(allPassAndLegs, pnrData) {
    var passByLeg = []
    allPassAndLegs.forEach(pass => {
        pass.FlightLegs.forEach(flightLeg => {
            var brdpass = new BoardPass(pnrData)
            brdpass.PAXID = pass.PAXID
            brdpass.PAXFirstName = pass.PAXFirstName
            brdpass.PAXLastName = pass.PAXLastName
            brdpass.PAXTitle = pass.PAXTitle
            brdpass.PAXInfant = pass.PAXInfant
            brdpass.PAXIsLead = pass.PAXIsLead

            brdpass.PAXTravelClass = flightLeg.PAXTravelClass
            brdpass.PAXSeatNum = flightLeg.PAXSeatNum
            brdpass.FlightNum = flightLeg.FlightNum
            brdpass.FlightOrigin = flightLeg.FlightOrigin
            brdpass.FlightOriginCity = flightLeg.FlightOriginCityName
            brdpass.FlightDestination = flightLeg.FlightDestination
            brdpass.FlightDestinationCity = flightLeg.FlightDestinationCityName
            brdpass.FlightDepDate = flightLeg.FlightDepartureDate
            brdpass.FlightDepTime = flightLeg.FlightDepartureTime
            brdpass.FlightArrDate = flightLeg.FlightArrivalDate
            brdpass.FlightArrTime = flightLeg.FlightArrivalTime
            brdpass.FlightDepGate = flightLeg.FlightDepGate
            brdpass.FlightBrdDate = flightLeg.FlightBoardingDate
            brdpass.FlightBrdTime = flightLeg.FlightBoardingTime
            brdpass.FlightBrdZone = flightLeg.FlightBoardingZone
            brdpass.TicketNumber = flightLeg.TicketNumber
            brdpass.SequenceNumber = flightLeg.SequenceNumber
            brdpass.barcode1 = flightLeg.LegBarcode1
            brdpass.barcode2 = flightLeg.LegBarcode2

            //console.log(brdpass)
            passByLeg.push(brdpass)
        })
    })

    return passByLeg
}


module.exports = BoardPassBuilder;