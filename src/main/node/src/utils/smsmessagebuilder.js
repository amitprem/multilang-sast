

function SMSMessageBuilder() {

}


SMSMessageBuilder.prototype.buildBoardingPassMessage = async (inputMessage) => {
    //console.log(inputMessage)
    let whatsappMessage = 'Hello... Please find your travel details below: \nPNR:' + inputMessage.PNR
    inputMessage.PAXs.forEach(passenger => {
        whatsappMessage = whatsappMessage + '\n---------------------------\n' + passenger.PAXTitle + '. ' + passenger.PAXFirstName + ' ' + passenger.PAXLastName
        passenger.FlightLegs.forEach(flightleg => {
            //console.log(flightleg)
            whatsappMessage = whatsappMessage + '\n---------------------------'
                + '\nTravel Date: ' + flightleg.FlightDepartureDate
                + '\nLeg: #' + flightleg.FlightLegID
                + '\nTravelling From: ' + flightleg.FlightOrigin + '; and Travelling To: ' + flightleg.FlightDestination
                + '\nBoarding at Origin: ' + flightleg.FlightBoardingDate + ' at ' + flightleg.FlightBoardingTime
                + '\nDeparture at Origin: ' + flightleg.FlightDepartureDate + ' at ' + flightleg.FlightDepartureTime
                + '\nArrival at Destination: ' + flightleg.FlightArrivalDate + ' at ' + flightleg.FlightArrivalTime
        })
    });

    return whatsappMessage
}

SMSMessageBuilder.prototype.buildAncillaryMessage = async (ancillaryData) => {
    let whatsappMessage = 'Hello... Your ancillary purchase for PNR:' + ancillaryData.PNR
    ancillaryData.EMDs.forEach(emd => {
        whatsappMessage = whatsappMessage + '\n---------------------------'
            + '\nDocument number: ' + emd.EmdNumber
            + '\nGuest name: ' + emd.EmdPAXName
            + '\nProduct purchased: ' + emd.EmdDescription
            + '\nPaid through: ' + emd.EmdFormOfPayment
            + '\nTotal price: ' + emd.EmdTotalCurrency + ' ' + emd.EmdTotalPrice
            + '\n---------------------------'
    })

    return whatsappMessage
}

module.exports = SMSMessageBuilder;