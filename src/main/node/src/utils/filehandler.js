const fs = require('fs')
const HandleBars = require('handlebars')
const pdf = require("html-pdf")
const dateFormat = require('dateformat');


function FileHandler() {
}

FileHandler.prototype.readFile = async (readFileInPath) => {
    const buffer = fs.readFileSync(readFileInPath, 'utf-8')
    //console.log(buffer);
    return buffer
}

FileHandler.prototype.writeFile = async (writeToFileInPath, contents) => {
    fs.writeFileSync(writeToFileInPath, contents, 'binary', (err) => {
        if (err) console.log(err);
        console.log("Successfully Written to File : " + writeToFileInPath);
    });
}


FileHandler.prototype.writeFileAsPDF = async (writeToFileInPath, contents) => {
    var options = { format: 'A4' };
    return new Promise((resolve, reject) => {
        console.log('Creating PDF :' + writeToFileInPath)
        pdf.create(contents, options).toFile(writeToFileInPath, (err, res) => {
            if (err) {
                console.log('Error in creating PDF:' + err)
                return reject(err)
            }
            
            console.log('Created PDF: ' + writeToFileInPath)
            resolve(res)
        });
    })
}

FileHandler.prototype.readFileForAttachment = (readFileInPath) => {
    const buffer = fs.readFileSync(readFileInPath).toString("base64")
    
    return buffer
}

module.exports = FileHandler;