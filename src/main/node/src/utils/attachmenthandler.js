const path = require('path');

const FileHandler = require('../utils/filehandler')

const fileHandler = new FileHandler()

function AttachmentHandler() {

}

AttachmentHandler.prototype.getAttachmentContent = (attachmentsDir, attachment) => {
    const attachFileContent = fileHandler.readFileForAttachment(path.join(attachmentsDir, attachment))

    var attachData = {}
    attachData.content = attachFileContent
    attachData.filename = attachment
    attachData.contentType = 'application/pdf'
    attachData.encoding = 'base64'

    return attachData
}

module.exports = AttachmentHandler