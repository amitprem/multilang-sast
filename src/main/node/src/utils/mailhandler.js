const nodemailer = require('nodemailer')
const HandleBars = require('handlebars')
const path = require('path');
const FileHandler = require('../utils/filehandler')
const base64Img = require('base64-img')

const fileHandler = new FileHandler()

const TemplatesDir = path.join(__dirname, '../../templates/boardingpass')

function MailHandler() {

}

const transporter = nodemailer.createTransport({
    // host: this.smtpHost,
    // port: this.smtpPort,
    // service: 'Gmail',
    // auth: {
    //     user: 'agrahara.subbu@gmail.com',
    //     pass: 'Logmeinn@13'
    // }

    host: 'smtp.zoho.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'agrahara.subbu@zohomail.com',
        pass: 'Logmeinn@13'
    }
})

MailHandler.prototype.sendEmail = async (travelData, attachments, handlebarfile, subject) => {
    if (!transporter) {
        transporter = nodemailer.createTransport({
            // host: this.smtpHost,
            // port: this.smtpPort,
            // service: 'Gmail',
            // auth: {
            //     user: 'agrahara.subbu@gmail.com',
            //     pass: 'Logmeinn@13'
            // }

            host: 'smtp.zoho.com',
            port: 465,
            secure: true, // use SSL
            auth: {
                user: 'agrahara.subbu@zohomail.com',
                pass: 'Logmeinn@13'
            }
        })
    }

    console.log('Using handlebar for email: ' + path.join(TemplatesDir, handlebarfile))

    const hbsBuffer = await fileHandler.readFile(path.join(TemplatesDir, handlebarfile))

    const template = await HandleBars.compile(hbsBuffer);

    const imageDir = path.join(__dirname, '../../images')
    const etihadLogo = path.join(imageDir, 'etihad-choose-well.png')

    const output = await template({
        "Reservation": travelData,
        "allowProtoMethodsByDefault": true,
        "etihadLogo": base64Img.base64Sync(etihadLogo)
    })

    //console.log(output)

    let mailOptions = {
        from: 'agrahara.subbu@zohomail.com',
        to: travelData.Recipient,
        subject,
        html: output,
        attachments
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log('Error Occured' + error);
            throw new Error(error);
        } else {
            console.log('Email message Sent: ' + info.response);
            return info
        }
    });
}

module.exports = MailHandler;