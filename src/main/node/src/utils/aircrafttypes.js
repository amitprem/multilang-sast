var fs = require('fs');
const path = require('path')

const configDir = path.join(__dirname, '../../config')
const acTypesJSONFile = path.join(configDir, 'aircraftypes.json')

function AircraftTypes () {

}

AircraftTypes.prototype.getAircraftTypeInfo = function ( aircraftType ) {
    var contents = fs.readFileSync(acTypesJSONFile);

    var AIRCRAFT_TYPE_MAP = JSON.parse(contents)
    //console.log('Finding subject for: ' + languageCode)
    let acTypeData = AIRCRAFT_TYPE_MAP.Data.filter(x => x.IATACode === aircraftType)
    
    if(acTypeData) {
        return acTypeData[0]
    }

    return undefined
}

module.exports = AircraftTypes