function PAXClassCode() {

    this.ECONOMY = ['T', 'E', 'U', 'V', 'L', 'Q', 'M', 'K', 'H', 'B', 'Y', 'N']

    this.BUSINESS = ['Z', 'W', 'D', 'C', 'J', 'I']

    this.FIRST = ['R', 'A', 'F', 'O']

    this.RESIDENCE = ['P']

}

PAXClassCode.prototype.getClassOfTravel = function (travelClassCode) {

    if (!travelClassCode) {
        return ''
    }

    if (this.ECONOMY.includes(travelClassCode)) {
        return 'ECONOMY'
    } else if (this.BUSINESS.includes(travelClassCode)) {
        return 'BUSINESS'
    } else if (this.FIRST.includes(travelClassCode)) {
        return 'FIRST'
    } else if (this.RESIDENCE.includes(travelClassCode)) {
        return 'RESIDENCE'
    } else {
        return 'NOT AVAILABLE'
    }
}

module.exports = PAXClassCode