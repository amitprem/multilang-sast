var fs = require('fs');
const path = require('path')

const configDir = path.join(__dirname, '../../config')
const airportsJSONFile = path.join(configDir, 'airports.json')

function AirportCityCode() {

}

AirportCityCode.prototype.getCityFromAirportCode = function (airportCode) {
        var AIRPORT_CITY_MAP = loadAirportCityMap()
        //console.log('Finding portname for: ' + airportCode)
        let airport = AIRPORT_CITY_MAP.Data.filter(x => x.Code === airportCode)
        let portName = ''
        if (airport) {
            //console.log('Found port: ' + airport)
            portName = airport[0].CityName;
        }

        //console.log('Returning portName: ' + portName)
        return portName
}

function loadAirportCityMap() {

    var contents = fs.readFileSync(airportsJSONFile);
    //console.log(contents);

    return JSON.parse(contents)
}

module.exports = AirportCityCode