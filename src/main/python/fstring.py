# new string formatting- Literal String Interpolation 
import datetime
name = 'Amit'
age = 40
anniversary = datetime.date(2004, 1, 4)
today = datetime.datetime.today() 

print (f"Hello My name is {name} and. Next yr i will b {age + 1}. My anniversary is  {anniversary:%A, %B %d, %Y}.")

print(f"Today is {today:%A,  %B %d, %Y}") 
