from flask import Flask
from flask import jsonify
import requests
import json
import datetime

app = Flask(__name__)

@app.route("/")
def index():
  log("home", "EY Test Python Container")
  return "EY Test Python Container"

@app.route("/myip")
def myip():
  x = requests.get('https://ifconfig.me')
  log("myip", x.text)
  return x.text


@app.route('/pnr/<string:pnr>/<string:lastname>', methods=['GET'])
def pnr(pnr, lastname):
    url = 'https://wci.etihad.com/api/wci/pnrsearch'
    data = {'lastName': 'nil', 'recordLocator': 'nil', 'searchType': 'PNR'}
    data["lastName"] = lastname
    data["recordLocator"] = pnr
    print(data)
    headers = {'Content-type': 'application/json', 'Accept': 'application/json', 'appid': 'wci.bff', 'sessionid': 'dce415d8-ff8c-4366-8c2b-0c658f60c647', 'tranid': '5a8c2f45-669a-404c-bf00-0621aed75f45', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36' }
    resp = requests.post(url, data=json.dumps(data), headers=headers)  
    log("pnr", resp.json())
    return resp.json()

def log(action, logobj):
    x = {"action":"-", "logtime":"-", "resp":"-", }  
    logtime =  datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S %z")
    x['logtime'] = logtime
    x['action']= action
    x['logobj']= logobj  
    print (json.dumps(x))


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
