import json

#numbers = [2,7,9,13,15,19]
numbers = {"One":1, "Two": 2, "Three":3, "Four":4}
filename = 'numbers.json'

with open(filename, 'w') as f:
    json.dump(numbers,f)
